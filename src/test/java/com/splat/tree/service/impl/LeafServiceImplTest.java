package com.splat.tree.service.impl;

import com.splat.tree.dto.WebixLeaf;
import com.splat.tree.entities.Leaf;
import com.splat.tree.repository.LeafRepository;
import com.splat.tree.util.NameGenerator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LeafServiceImplTest {

    @Mock
    private LeafRepository leafRepositoryMock;

    @InjectMocks
    private LeafServiceImpl target = new LeafServiceImpl();

    @Test
    public void findChildByParentTest() throws Exception {
        when(leafRepositoryMock.findByParentIsNull()).thenReturn(Collections.emptyList());
        Leaf leaf = new Leaf();
        Leaf children = new Leaf();
        children.setId("cId");
        children.setValue("cValue");
        leaf.setChildren(Collections.singletonList(children));
        when(leafRepositoryMock.findById("TestId")).thenReturn(Optional.of(leaf));
        List<WebixLeaf> childByParent = target.findChildByParent(NameGenerator.ROOT_ID);
        assert childByParent.isEmpty();
        verify(leafRepositoryMock, only()).findByParentIsNull();
        List<WebixLeaf> testId = target.findChildByParent("TestId");
        assert testId.size() == 1;
        verify(leafRepositoryMock, times(1)).findById("TestId");
        verify(leafRepositoryMock, times(1)).countByParent(children);
    }

    @Test
    public void createTest() throws Exception {
        when(leafRepositoryMock.findById("test")).thenReturn(Optional.empty());
        when(leafRepositoryMock.save(any(Leaf.class))).thenReturn(new Leaf());
        target.create("test");
        verify(leafRepositoryMock, times(1)).findById("test");
        verify(leafRepositoryMock, times(1)).save(any(Leaf.class));
    }

    @Test
    public void removeTest() throws Exception {
        target.remove("test");
        verify(leafRepositoryMock, times(1)).delete("test");
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void updateWhenLeafIsNotFoundTest() throws Exception {
        when(leafRepositoryMock.findById("test")).thenReturn(Optional.empty());
        target.update(new WebixLeaf().withId("test"));
    }

}
