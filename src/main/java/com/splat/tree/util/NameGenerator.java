package com.splat.tree.util;

import java.text.MessageFormat;
import java.util.Optional;

public final class NameGenerator {

    public static final String ROOT_ID = "1";

    private static final String DEFAULT_FILE_NAME = "Part {0}";

    private NameGenerator() {}

    public static String generateByParent(Optional<String> parentName, long countChild) {
        return parentName.map(name -> MessageFormat.format("{0}.{1}", name, countChild + 1))
                .orElse(MessageFormat.format(DEFAULT_FILE_NAME, countChild + 1));
    }
}
