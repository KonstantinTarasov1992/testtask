package com.splat.tree.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "leaves")
public class Leaf {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Column(columnDefinition = "CHAR(32)")
    private String id;
    private String value;
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private List<Leaf> children;

    @JoinColumn(name = "parentId", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Leaf parent;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Leaf> getChildren() {
        return children;
    }

    public void setChildren(List<Leaf> children) {
        this.children = children;
    }

    public Leaf getParent() {
        return parent;
    }

    public void setParent(Leaf parent) {
        this.parent = parent;
    }
}
