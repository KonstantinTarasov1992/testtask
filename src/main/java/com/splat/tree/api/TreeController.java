package com.splat.tree.api;

import com.splat.tree.dto.WebixFolder;
import com.splat.tree.dto.WebixLeaf;
import com.splat.tree.service.LeafService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/tree")
public class TreeController {

    @Autowired
    private LeafService leafService;

    @RequestMapping(method = RequestMethod.GET)
    public WebixFolder getFolder(@RequestParam(name = "parent", required = false) String parentId) throws InterruptedException {
        List<WebixLeaf> webxLeaves = leafService.findChildByParent(parentId);
        Thread.sleep(2000);
        return new WebixFolder().withParent(parentId).withData(webxLeaves);
    }

    @RequestMapping(method = RequestMethod.POST)
    public WebixLeaf create(@RequestParam("parent") String parentId) {
        return leafService.create(parentId);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void update(@RequestBody WebixLeaf webixLeaf) {
        leafService.update(webixLeaf);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public void removeLeaf(@PathVariable String id) {
        leafService.remove(id);
    }

    @RequestMapping(path = "/{id}/move", method = RequestMethod.POST)
    public void moveLeaf(@PathVariable String id, @RequestParam(value = "parent", required = false) String parent) {
        leafService.move(id, parent);
    }
}
