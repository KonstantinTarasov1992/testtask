package com.splat.tree.service;

import com.splat.tree.dto.WebixLeaf;

import java.util.List;

public interface LeafService {
    /**
     * find leaves by parent
     * @param parentId - parent id
     * @return founded leaves
     */
    List<WebixLeaf> findChildByParent(String parentId);

    /**
     * Create new leaf
     * @param parentId - parent id
     * @return webix leaf
     */
    WebixLeaf create(String parentId);

    /**
     * Remove leaf by id
     * @param id
     */
    void remove(String id);

    /**
     * Update leaf
     * @param webixLeaf
     */
    void update(WebixLeaf webixLeaf);

    /**
     * Move leaf to new parent
     * @param id - leaf id
     * @param parentId - new parent id
     */
    void move(String id, String parentId);
}
