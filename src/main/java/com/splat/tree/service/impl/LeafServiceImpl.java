package com.splat.tree.service.impl;

import com.splat.tree.dto.WebixLeaf;
import com.splat.tree.entities.Leaf;
import com.splat.tree.repository.LeafRepository;
import com.splat.tree.service.LeafService;
import com.splat.tree.util.NameGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.splat.tree.util.NameGenerator.ROOT_ID;

@Service
public class LeafServiceImpl implements LeafService {

    @Autowired
    private LeafRepository leafRepository;

    @Override
    public List<WebixLeaf> findChildByParent(String parentId) {
        Stream<Leaf> leaves = Objects.equals(parentId, ROOT_ID) ? leafRepository.findByParentIsNull().stream() :
                leafRepository.findById(parentId).map(Leaf::getChildren).map(Collection::stream).orElse(Stream.empty());
        return leaves.map(this::toWebxLeaf).sorted(this::compareByVaue).collect(Collectors.toList());
    }

    @Override
    public WebixLeaf create(String parentId) {
        Optional<Leaf> parent = leafRepository.findById(parentId);
        Leaf leaf = new Leaf();
        parent.ifPresent(leaf::setParent);
        String value = NameGenerator.generateByParent(parent.map(Leaf::getValue),
                leafRepository.countByParent(parent.orElse(null)));
        leaf.setValue(value);
        Leaf savedLeaf = leafRepository.save(leaf);
        return new WebixLeaf().withId(savedLeaf.getId())
                .withValue(savedLeaf.getValue());
    }

    @Override
    public void remove(String id) {
        leafRepository.delete(id);
    }

    @Override
    public void update(WebixLeaf webixLeaf) {
        Leaf leaf = leafRepository.findById(webixLeaf.getId()).orElseThrow(() -> new EmptyResultDataAccessException(MessageFormat
                .format("No leaf with id {0} exists!", webixLeaf.getId()), 1));
        leaf.setValue(webixLeaf.getValue());
        leafRepository.save(leaf);
    }

    @Override
    public void move(String id, String parentId) {
        Optional<Leaf> leafOpt = leafRepository.findById(id);
        Leaf leaf = leafOpt.orElseThrow(() -> new EmptyResultDataAccessException(MessageFormat
                .format("No leaf with id {0} exists!", id), 1));
        Leaf parent = ROOT_ID.equals(parentId) ? null : leafRepository.findById(parentId).orElseThrow(() ->
                new EmptyResultDataAccessException(MessageFormat
                .format("No parent with id {0} exists!", parentId), 1));
        leaf.setParent(parent);
        leafRepository.save(leaf);
    }

    private int compareByVaue(WebixLeaf leaf1, WebixLeaf leaf2) {
        if(leaf1.getValue() == null && leaf2.getValue() == null) {
            return 0;
        } else if(leaf1.getValue() == null && leaf2.getValue() != null) {
            return -1;
        } else if(leaf1.getValue() != null && leaf2.getValue() == null) {
            return 1;
        }
        else {
            return leaf1.getValue().compareTo(leaf2.getValue());
        }

    }

    private WebixLeaf toWebxLeaf(Leaf leaf) {
        return new WebixLeaf()
                .withId(leaf.getId())
                .withValue(leaf.getValue())
                .withKids(leafRepository.countByParent(leaf) > 0);
    }
}
