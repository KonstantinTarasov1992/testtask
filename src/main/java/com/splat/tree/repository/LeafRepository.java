package com.splat.tree.repository;

import com.splat.tree.entities.Leaf;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LeafRepository extends CrudRepository<Leaf, String> {
    Optional<Leaf> findById(String id);
    List<Leaf> findByParentIsNull();
    long countByParent(Leaf parent);
}
