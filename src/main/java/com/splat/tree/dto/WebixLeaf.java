package com.splat.tree.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WebixLeaf {
    private String id;
    private String value;
    @JsonProperty(value = "webix_kids")
    private boolean kids;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public WebixLeaf withId(String id) {
        setId(id);
        return this;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public WebixLeaf withValue(String value) {
        setValue(value);
        return this;
    }

    public boolean isKids() {
        return kids;
    }

    public void setKids(boolean kids) {
        this.kids = kids;
    }

    public WebixLeaf withKids(boolean kids) {
        setKids(kids);
        return this;
    }
}
