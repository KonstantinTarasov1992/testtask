package com.splat.tree.dto;


import java.util.List;

public class WebixFolder {
    private String parent;
    private List<WebixLeaf> data;

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public WebixFolder withParent(String parentId) {
        setParent(parentId);
        return this;
    }

    public List<WebixLeaf> getData() {
        return data;
    }

    public void setData(List<WebixLeaf> data) {
        this.data = data;
    }

    public WebixFolder withData(List<WebixLeaf> data) {
        setData(data);
        return this;
    }
}
